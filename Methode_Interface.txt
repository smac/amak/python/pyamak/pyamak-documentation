Méthode d'interface :

A) Côté Noyau vers Côté Interface Graphique :
 - makeCycle() -> entrée : état n (environnement, amas, ...)
	          sortie : état n+1 (environnement, amas, ...)

B) Côté Interface Graphique vers Côté Noyau :
 - affichageEtat() -> entrée : état (environnement, amas, ...)

 - initAgents() : entrée état (environnement, amas, ...)

 - updateAgent() : entrée état (environnement, amas, ...)

 - drawRectangle(), drawLine(), drawCircle()

 - begin()/stop()

 - accelerate()